package p1;

public class Rectangle {
	  private int x, y;
	    private int width, height;

	    public Rectangle() {
	    this(12, 12);
	        System.out.println("inside default");
	       
	    }
	    public Rectangle(int width, int height) {
	      this( 10,10,width, height);
	        System.out.println("inside cons");
	    }
	    public Rectangle(int x, int y, int width, int height) {
	    	System.out.println("inside cons1");
	    	this.x = x;
	        this.y = y;
	        this.width = width;
	        this.height = height;
	        System.out.println("inside cons1");
	       
	    }
	    public static void main(String[] args) {
	    Rectangle r=new Rectangle();
	    System.out.println(r.height);
	 	   System.out.println(r.x);
	 	   System.out.println(r.y);
	 	   System.out.println(r.width);
	 	  System.out.println(Math.min(Integer.MIN_VALUE, 0));
	 	 System.out.println(Math.min(Double.MIN_VALUE, 0.0d));
	    }
	
	    
	/*
	 * public static void main(String[] args) { outerloop: for (int i=0; i < 5;
	 * i++) { inner: for (int j=0; j < 5; j++) { if (i * j > 6) {
	 * System.out.println("Breaking"); break outerloop; } System.out.println(i +
	 * " " + j); } } System.out.println("Done"); }
	 */

	/*
	 * public static void main(String args[]){ Task: for(int i=0; i<10; i++){ if
	 * (i==8){
	 * 
	 * continue Task; } System.out.println("......."+i ); }
	 * System.out.println("hii"); }
	 */

}