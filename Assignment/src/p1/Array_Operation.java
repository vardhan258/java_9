package p1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;



public class Array_Operation {

	private static void sortByFrequency(int[] arr) {
		Map<Integer, Integer> frequencyMap = createFrequencyMap(arr);
		List<Entry<Integer, Integer>> entryList = sortByValue(frequencyMap);
		putSortedElementsBackInArray(arr, entryList);

	}

	private static Map<Integer, Integer> createFrequencyMap(int[] arr) {
		Map<Integer, Integer> frequencyMap = new LinkedHashMap<>();
		for (int i = 0; i < arr.length; i++) {
			int key = arr[i];
			if (frequencyMap.containsKey(key)) {
				frequencyMap.put(key, frequencyMap.get(key) + 1);
			} else {
				frequencyMap.put(key, 1);
			}
		}
		// System.out.println("^^^^^^^^"+frequencyMap);
		return frequencyMap;
	}

	private static List<Entry<Integer, Integer>> sortByValue(Map<Integer, Integer> frequencyMap) {
		List<Entry<Integer, Integer>> entryList = new ArrayList<Entry<Integer, Integer>>(frequencyMap.entrySet());
    Collections.sort(entryList, new Comparator<Map.Entry<Integer, Integer>>() {
			public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});
		// System.out.println("\n----+---"+entryList);
		return entryList;
	}

	private static void putSortedElementsBackInArray(int[] arr, List<Entry<Integer, Integer>> list) {
		int index = 0;
		// System.out.println("inside putSortedElementsBackInArray");
		// System.out.println("### "+Arrays.toString(arr));
		// System.out.println("------"+list);
		// Arrange array elements in sorted list of entry set of frequency map.
		for (Map.Entry<Integer, Integer> entry : list) {
			for (int i = 0; i < entry.getValue(); i++) {
				// System.out.println("==== "+entry.getValue());
			// System.out.println("****** "+entry.getKey());
				arr[index++] = entry.getKey();
				// System.out.println(Arrays.toString(arr));
			}
		}
	}

	public static void main(String[] args) {
		int[] arr = { 2, 5, 3, 8, 7, 2, 5, 2, 3, 3, 5, 3, 1, 0 };

		System.out.println("Input array before sorting elements by frequency.");
		System.out.println(Arrays.toString(arr));
		Arrays.sort(arr);
		sortByFrequency(arr);
		System.out.println("Array after sorting elements by frequency.");
		System.out.println(Arrays.toString(arr));
	}
}