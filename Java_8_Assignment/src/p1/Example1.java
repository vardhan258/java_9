package p1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Example1 {

	public static void main(String[] args) {
	List<String> alist=new ArrayList<String>();
		alist.add("namit");
		alist.add("nam");
		alist.add("amit");
		alist.add("mit");
	System.out.println(alist);
	
	/*for each*/	
	for(String s:alist)
	System.out.println(s);
	
	Example1 ex=new Example1();
	/*using java 7*/
	ex.SortValue(alist);
	System.out.println("*********"+alist);
	
	//using java 8
	ex.SortValue1(alist);
	System.out.println("-----"+alist);
	

	
	
	
	}
	private void SortValue1(List<String> alist1) {
		
		
		Collections.sort(alist1, (s1,s2)->s1.compareTo(s2));
		
		
		
		
	}
	public void SortValue(List<String> alist){
	Collections.sort(alist, new Comparator<String>() {
     
        public int compare(String s1, String s2) {
           return s1.compareTo(s2);
        }

	});
	}
}