package p1;

public class Functional_expression {
public static void main(String[] args) {
	Thread t=new Thread(new Runnable() 
    { 
        public void run() 
        { 
            System.out.println("Child Thread"); 
        } 
    });

t.start();
System.out.println("main Thread");

}
}
