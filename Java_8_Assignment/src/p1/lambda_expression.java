package p1;

public class lambda_expression {
	public static void main(String args[]) {
		lambda_expression tester = new lambda_expression();
			
	      System.out.println("10 + 5 = " + tester.operate(10, 5, (a,b) -> a + b));
	      System.out.println("10 - 5 = " + tester.operate(10, 5, (a, b) -> a - b));
	      System.out.println("10 x 5 = " + tester.operate(10, 5, (a,b) -> a * b ));
	      System.out.println("10 / 5 = " + tester.operate(10, 5, ( a, b) -> a / b));
			
	      //without parenthesis
	      GreetingService greetService1 = message ->
	      System.out.println("Hello " + message);
			
	      //with parenthesis
	      GreetingService greetService2 = (message) ->
	      System.out.println("Hello " + message);
			
	      greetService1.sayMessage("Mahesh");
	      greetService2.sayMessage("Suresh");
	   }
		
	   interface MathOperation {
	      int operation(int a, int b);
	   }
		
	   interface GreetingService {
	      void sayMessage(String message);
	   }
		
	   private int operate(int a, int b, MathOperation mathOperation) {
	      return mathOperation.operation(a, b);
	   }

}
